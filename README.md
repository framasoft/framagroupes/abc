[![](https://framasoft.org/nav/img/icons/ati/sites/git.png)](https://framagit.org)

🇬🇧 **Framasoft uses GitLab** for the development of its free softwares. Our GitHub repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your GitHub account)

🇫🇷 **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts GitHub ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l’inscription n’est pas nécessaire, vous pouvez vous connecter avec votre compte GitHub)
* * *

Framalistes est un service en ligne de liste de discussion que Framasoft propose sur le site : https://framalistes.org

Il repose sur le logiciel [Sympa](https://www.sympa.org).

Ce dépôt ne contient pas Sympa, juste la page d’accueil.
* * *
